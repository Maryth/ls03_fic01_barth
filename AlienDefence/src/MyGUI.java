import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.plaf.ColorChooserUI;

//import com.sun.tools.sjavac.comp.dependencies.PublicApiCollector;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMainText = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblMainText.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMainText.setHorizontalAlignment(SwingConstants.CENTER);
		lblMainText.setBounds(10, 43, 414, 14);
		contentPane.add(lblMainText);
		
		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(10, 139, 130, 23);
		contentPane.add(btnRed);
		
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnBlue.setBounds(294, 139, 130, 23);
		contentPane.add(btnBlue);
		
		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_clicked();
			}
		});
		btnGreen.setBounds(154, 139, 130, 23);
		contentPane.add(btnGreen);
		
		JLabel lblHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblHintergrundfarbe.setBounds(10, 114, 274, 14);
		contentPane.add(lblHintergrundfarbe);
		
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_clicked();
			}
		});
		btnYellow.setBounds(10, 173, 130, 23);
		contentPane.add(btnYellow);
		
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandard_clicked();
			}
		});
		btnStandard.setBounds(154, 173, 130, 23);
		contentPane.add(btnStandard);
		
		JButton btnCustomColor = new JButton("Farbe w\u00E4hlen");
		btnCustomColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCustomColor_clicked();
			}
		});
		btnCustomColor.setBounds(294, 173, 130, 23);
		contentPane.add(btnCustomColor);
		
		JLabel lblNewLabel = new JLabel("Aufgabe 2: Text formatieren");
		lblNewLabel.setBounds(10, 207, 169, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblMainText.setFont(new Font("Arial", Font.PLAIN,lblMainText.getFont().getSize()));

			}
		});
		btnArial.setBounds(10, 232, 130, 23);
		contentPane.add(btnArial);
		
		JButton btnComic = new JButton("Comic Sans MS");
		btnComic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setFont(new Font("Comic Sans MS", Font.PLAIN,lblMainText.getFont().getSize()));
			}
		});
		btnComic.setBounds(154, 232, 130, 23);
		contentPane.add(btnComic);
		
		JButton btnCourier = new JButton("Courier New");
		btnCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setFont(new Font("Courier New", Font.PLAIN, lblMainText.getFont().getSize()));
			}
		});
		btnCourier.setBounds(294, 232, 130, 23);
		contentPane.add(btnCourier);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setToolTipText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 266, 414, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnsetLabel = new JButton("Ins Label schreiben");
		btnsetLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setText(txtHierBitteText.getText());
			}
		});
		btnsetLabel.setBounds(10, 297, 200, 23);
		contentPane.add(btnsetLabel);
		
		JButton btnDeleteLabel = new JButton("Text im Label l\u00F6schen");
		btnDeleteLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setText("");
			}
		});
		btnDeleteLabel.setBounds(224, 297, 200, 23);
		contentPane.add(btnDeleteLabel);
		
		JLabel lblchangeFont = new JLabel("Aufgabe 3: Schirftfarbe \u00E4ndern");
		lblchangeFont.setBounds(10, 332, 157, 14);
		contentPane.add(lblchangeFont);
		
		JButton btnRedFont = new JButton("Rot");
		btnRedFont.setToolTipText("Bitte hier Text eingeben");
		btnRedFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setForeground(Color.RED);

			}
		});
		btnRedFont.setBounds(10, 357, 130, 23);
		contentPane.add(btnRedFont);
		
		JButton btnBlueFont = new JButton("Blau");
		btnBlueFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setForeground(Color.BLUE);
			}
		});
		btnBlueFont.setBounds(154, 357, 130, 23);
		contentPane.add(btnBlueFont);
		
		JButton btnBlackFont = new JButton("Schwarz");
		btnBlackFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblMainText.setForeground(Color.BLACK);
			}
		});
		btnBlackFont.setBounds(294, 357, 130, 23);
		contentPane.add(btnBlackFont);
		
		JLabel lblChangeFontSize = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblChangeFontSize.setBounds(10, 391, 200, 14);
		contentPane.add(lblChangeFontSize);
		
		JButton btnBiggerFont = new JButton("+");
		btnBiggerFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblMainText.getFont().getSize();
				lblMainText.setFont(new Font(lblMainText.getFont().getFontName(), Font.PLAIN, groesse +1));
			}
		});
		btnBiggerFont.setBounds(10, 416, 200, 23);
		contentPane.add(btnBiggerFont);
		
		JButton btnSmallerFont = new JButton("-");
		btnSmallerFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblMainText.getFont().getSize();
				lblMainText.setFont(new Font(lblMainText.getFont().getFontName(), Font.PLAIN, groesse -1));
			}
		});
		btnSmallerFont.setBounds(224, 416, 200, 23);
		contentPane.add(btnSmallerFont);
		
		JLabel lblChangeTextConstrains = new JLabel("Aufgabe 5: Textausrichtung");
		lblChangeTextConstrains.setBounds(10, 450, 200, 14);
		contentPane.add(lblChangeTextConstrains);
		
		JButton btnLeft = new JButton("linksb\u00FCndig");
		btnLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLeft.setBounds(10, 475, 130, 23);
		contentPane.add(btnLeft);
		
		JButton btnCenter = new JButton("Zentriert");
		btnCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnCenter.setBounds(154, 475, 130, 23);
		contentPane.add(btnCenter);
		
		JButton btnRight = new JButton("rechtsb\u00FCndig");
		btnRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMainText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRight.setBounds(294, 475, 130, 23);
		contentPane.add(btnRight);
		
		JLabel lblExit = new JLabel("Aufgabe 6: Programm beenden");
		lblExit.setBounds(10, 509, 212, 14);
		contentPane.add(lblExit);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 534, 414, 47);
		contentPane.add(btnExit);

	}


	public void buttonCustomColor_clicked() {
		// TODO Auto-generated method stub
	getContentPane().setBackground(JColorChooser.showDialog(null, "Farbe w�hlen", Color.white));
	}

	public void buttonStandard_clicked() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(new Color(0xEEEEEE)); 
	}

	public void buttonYellow_clicked() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.YELLOW);
	}

	public void buttonGreen_clicked() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.GREEN);
	}

	public void buttonBlue_clicked() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.BLUE);
	}

	public void buttonRed_clicked() {
		// TODO Auto-generated method stub
		this.contentPane.setBackground(Color.RED);
	}
}
